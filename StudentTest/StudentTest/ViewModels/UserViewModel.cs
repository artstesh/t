﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StudentTest.ViewModels
{
    public class UserViewModel
    {
        public int Id { get; set; }
        public string Login { get; set; }
    }
}