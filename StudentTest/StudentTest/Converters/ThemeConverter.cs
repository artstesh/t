﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StudentTest.Models;
using StudentTest.ViewModels;

namespace StudentTest.Converters
{
    public static class ThemeConverter
    {
        public static PrestartViewModel ToModel(this List<Theme> themes, string sessionGuid)
        {
            PrestartViewModel viewModel = new PrestartViewModel();
            viewModel.Themes = new List<Theme>();
            viewModel.SessionGuid = sessionGuid;
            //viewModel.First = first;

            //viewModel.Themes.AddRange(themes.ToArray());
            foreach (var theme in themes)
            {
                viewModel.Themes.Add(theme);
            }

            return viewModel;
        }
    }
}