using System.ComponentModel.DataAnnotations.Schema;

namespace StudentTest.Models
{
    [Table("user_result")]
    public class UserResult : BaseEntity
    {
        [Column("SessionGuid")]
        public string SessionGuid { get; set; }
        [Column("ThemeId")]
        public int ThemeId { get; set; }
        [Column("QuestionId")]
        public int QuestionId { get; set; }
        [Column("AnswerId")]
        public int AnswerId { get; set; }

        public virtual Question Question { get; set; }
        public virtual Answer Answer { get; set; }
    }
}