using System.ComponentModel.DataAnnotations.Schema;

namespace StudentTest.Models
{
    public abstract class BaseEntity
    {
        [Column("Id")]
        public int Id { get; set; }
    }
}