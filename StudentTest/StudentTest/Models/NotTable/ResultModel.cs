using System.Collections.Generic;

namespace StudentTest.Models.NotTable
{
    public class ResultModel
    {
        public int ThemeId { get; set; }
        public int QuestionId { get; set; }
        public string SessionGuid { get; set; }
        public List<int> Answers { get; set; }
    }
}