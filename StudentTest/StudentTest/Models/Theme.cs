﻿using System.ComponentModel.DataAnnotations.Schema;

namespace StudentTest.Models
{
    [Table("theme")]
    public class Theme : BaseEntity
    {
        [Column("Name")]
        public string Name { get; set; }
        [Column("Description")]
        public string Description { get; set; }
    }
}