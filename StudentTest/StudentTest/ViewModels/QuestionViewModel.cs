using System.Collections.Generic;

namespace StudentTest.ViewModels
{
    public class QuestionViewModel
    {
        public int QuestionId { get; set; }
        public string QuestionText { get; set; }
        public string SessionGuid { get; set; }
        public Dictionary<int, string> Answers { get; set; }
    }
}