﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using StudentTest.DbContext;
using StudentTest.Models;
using StudentTest.ViewModels;
using StudentTest.Converters;
using StudentTest.Services.S_G_Ch;
using System.Web.Mvc;

namespace StudentTest.Services
{
    public class ThemeService
    {
        private readonly IDataContext _context;

        public ThemeService(IDataContext context = null)
        {
            _context = context ?? new DataContext();
        }

        public PrestartViewModel GetThemes(string sessionGuid)
        {
            var themes = _context.Themes.ToList();
            return themes.ToModel(sessionGuid);
        }
    }
}