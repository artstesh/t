﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StudentTest.Models;
using StudentTest.ViewModels;
using StudentTest.DbContext;

namespace StudentTest.Converters
{
    public static class SessionConverter
    {
        /// https://metanit.com/sharp/tutorial/3.18.php
        /// https://metanit.com/sharp/tutorial/4.5.php

        public static List<SessionViewModel> ToModel(this List<string> sessionGuids)
        {
            List<SessionViewModel> viewModel = new List<SessionViewModel>();

            DataContext _context = new DataContext();

            foreach (var sessionGuid in sessionGuids)
            {
                viewModel.Add(new SessionViewModel { SessionGuid = sessionGuid,
                    DateTime = _context.Sessions.Where(g => g.Guid == sessionGuid).Select(d => d.Date).FirstOrDefault()
                });
            }

            return viewModel;
        }
    }
}