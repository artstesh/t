﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StudentTest.DbContext;
using StudentTest.Models;
using StudentTest.ViewModels;
using StudentTest.Converters;
using StudentTest.Services.S_G_Ch;
using System.Web.Mvc;

namespace StudentTest.Services
{
    public class LoginService
    {
        private readonly IDataContext _context;

        public LoginService(IDataContext context = null)
        {
            _context = context ?? new DataContext();
        }

        public List<Group> GetGroupsAtLoginPage()
        {
            List<Group> groups = _context.Groups.ToList();
            return groups;
        }
    }
}