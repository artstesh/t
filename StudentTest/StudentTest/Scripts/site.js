function onAnswerClick(id) {
    $("#answer-"+id).toggleClass("selected-answer");
}

function next() {
    var questionId = $("#question-id").text();
    var sessionGuid = $("#session-guid").text();
    var answers = $(".selected-answer");
    if (!answers || !answers.length) {
        alert("Ответ на вопрос не выбран");
        return;
    }
        /*return alert("Ответ на вопрос не выбран");*/
    var answerIds = [];
    answer = answers.each(function() {
        return answerIds.push(this.id.replace("answer-",""));
    });
    $.ajax({
        type: "POST",
        url: "/home/test",
        data: JSON.stringify({
            "QuestionId": questionId,
            "SessionGuid": sessionGuid, 
            "Answers" : answerIds
        }),
        contentType: "application/json; charset=utf-8"
    }).done(
        window.location.href = "/home/test?sessionGuid=" + sessionGuid
    );
}