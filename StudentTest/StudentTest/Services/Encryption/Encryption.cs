﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Cryptography;
using System.Text;


namespace StudentTest.Services.Encryption
{
    public static class Encryption
    {
        // метод генерации ключа сессии
        public static string GenerateRandomString(int length)
        {
            var rnd = new Random(Environment.TickCount);
            var str = string.Empty;

            while (length-- > 0)
            {
                str += (char)rnd.Next(33, 127);
            }

            return str;
        }

        // метод сравнения хешей паролей
        public static bool Check_Password(string password, string PasswordInDB)
        {
            if (GetHashString(password).Equals(PasswordInDB))
            {
                return true;
            }
            else return false;
        }

        public static string GetHashString(string inputString)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in GetHash(inputString))
                sb.Append(b.ToString("X2"));

            return sb.ToString();
        }

        public static byte[] GetHash(string inputString)
        {
            HashAlgorithm algorithm = SHA256.Create();
            return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
        }
    }
}