﻿using System.Data.Entity;
using System.Threading.Tasks;
using StudentTest.DbContext;
using StudentTest.Models;

namespace StudentTest.Test.Context
{
    internal class FakeContext : IDataContext
    {
        public FakeContext()
        {
            Answers = new FakeDbSet<Answer>();
            Questions = new FakeDbSet<Question>();
            Users = new FakeDbSet<User>();
            Sessions = new FakeDbSet<Session>();
            SessionQuestions = new FakeDbSet<SessionQuestion>();
            UserResults = new FakeDbSet<UserResult>();
        }

        public void Dispose()
        {
        }

        public DbSet<Answer> Answers { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Session> Sessions { get; set; }
        public DbSet<SessionQuestion> SessionQuestions { get; set; }
        public DbSet<UserResult> UserResults { get; set; }

        public async Task<int> SaveChangesAsync()
        {
            return 1;
        }

        public int SaveChanges()
        {
            return 1;
        }
    }
}