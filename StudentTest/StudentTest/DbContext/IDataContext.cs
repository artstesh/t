﻿using System;
using System.Data.Entity;
using System.Threading.Tasks;
using StudentTest.Models;

namespace StudentTest.DbContext
{
    public interface IDataContext : IDisposable
    {
        DbSet<Answer> Answers { get; set; }
        DbSet<Question> Questions { get; set; }
        DbSet<User> Users { get; set; }
        DbSet<Session> Sessions { get; set; }
        DbSet<SessionQuestion> SessionQuestions { get; set; }
        DbSet<UserResult> UserResults { get; set; }
        DbSet<Group> Groups { get; set; }
        DbSet<Theme> Themes { get; set; }

        Task<int> SaveChangesAsync();
        int SaveChanges();
    }
}