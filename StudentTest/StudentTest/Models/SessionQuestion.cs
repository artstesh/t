using System.ComponentModel.DataAnnotations.Schema;

namespace StudentTest.Models
{
    [Table("session_question")]
    public class SessionQuestion : BaseEntity
    {
        [Column("SessionId")]
        public int SessionId { get; set; }
        [Column("SessionGuid")]
        public string SessionGuid { get; set; }
        [Column("QuestionId")]
        public int QuestionId { get; set; }
        [Column("ThemeId")]
        public int ThemeId { get; set; }

        public virtual Question Question { get; set; }
    }
}