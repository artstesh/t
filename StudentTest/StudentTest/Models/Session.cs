using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace StudentTest.Models
{
    [Table("session")]
    public class Session : BaseEntity
    {
        [Column("Guid")]
        public string Guid { get; set; }
        [Column("Date")]
        public DateTime Date { get; set; }
        [Column("UserId")]
        public int UserId { get; set; }

        public virtual User User { get; set; }
    }
}