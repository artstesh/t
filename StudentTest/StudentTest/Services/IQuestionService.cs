using StudentTest.ViewModels;
//using System.Collections.Generic;
//using StudentTest.Models;

namespace StudentTest.Services
{
    public interface IQuestionService
    {
        QuestionViewModel StartSession(string sessionGuid, int themeId, int length = 10);
        QuestionViewModel NextQuestion(string sessionGuid);
    }
}