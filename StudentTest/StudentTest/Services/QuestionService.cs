using System;
using System.Collections.Generic;
using System.Linq;
using StudentTest.Converters;
using StudentTest.DbContext;
using StudentTest.Models;
using StudentTest.ViewModels;

namespace StudentTest.Services
{
    public class QuestionService : IQuestionService
    {
        private readonly IDataContext _context;

        public QuestionService(IDataContext context = null)
        {
            _context = context ?? new DataContext();
        }

        public QuestionViewModel StartSession(string sessionGuid, int themeId, int length = 10)
        {
            var questions = _context.Questions
                .Where(q => q.ThemeId == themeId)
                .Select(q => q).ToList()
                .OrderBy(o => Guid.NewGuid().ToString("N"))
                .Take(length).ToList();
            var returning = questions.First();
            questions.Remove(returning);
            var sessionQuestions = questions.Select(q => new SessionQuestion
            {
                QuestionId = q.Id, SessionGuid = sessionGuid, ThemeId = themeId
            });
            _context.SessionQuestions.AddRange(sessionQuestions);
            _context.SaveChanges();
            return returning.ToModel(sessionGuid);
        }

        public QuestionViewModel NextQuestion(string sessionGuid)
        {
            var sessionQuestions = _context.SessionQuestions.Where(sq => sq.SessionGuid == sessionGuid).ToList();
            var toRemove = sessionQuestions.FirstOrDefault();
            if (toRemove == null) return null;
            var returning = toRemove.Question;
            _context.SessionQuestions.Remove(toRemove);
            return returning.ToModel(sessionGuid);
        }

        public void SaveResult(List<UserResult> usrReslts)
        {
            _context.UserResults.AddRange(usrReslts);
            _context.SaveChanges();
        }
    }
}