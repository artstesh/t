using System.ComponentModel.DataAnnotations.Schema;

namespace StudentTest.Models
{
    [Table("user")]
    public class User : BaseEntity
    {
        [Column("Login")]
        public string Login { get; set; }
        [Column("Password")]
        public string Password { get; set; }
        [Column("GroupId")]
        public int GroupId { get; set; }

        public virtual Group Group { get; set; }
    }
}