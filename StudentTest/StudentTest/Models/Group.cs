﻿using System.ComponentModel.DataAnnotations.Schema;

namespace StudentTest.Models
{
    [Table("group")]
    public class Group : BaseEntity
    {
        [Column("Name")]
        public string Name { get; set; }
    }
}