﻿using System.Collections.Generic;
using System.Linq;
using StudentTest.Models;
using StudentTest.ViewModels;
using StudentTest.DbContext;
using StudentTest.Models.NotTable;

namespace StudentTest.Converters
{
    public static class UserResultConverter
    {
        /// https://metanit.com/sharp/tutorial/3.18.php
        /// https://metanit.com/sharp/tutorial/4.5.php

        public static List<UserResult> ToEntity(this ResultModel viewModel)
        {
            return viewModel.Answers.Select(answerId => new UserResult
            {
                ThemeId = viewModel.ThemeId,
                SessionGuid = viewModel.SessionGuid,
                QuestionId = viewModel.QuestionId,
                AnswerId = answerId
            }).ToList();
        }

        public static List<ResultViewModel> ToModel(this List<UserResult> userResults, List<Question> correctResults)
        {
            var viewModel = correctResults.Select(q =>
            {
                return new ResultViewModel
                {
                    QuestionText = q.Text,
                    Answers = q.Answers.Select(a =>
                    {
                        var neutral = !a.IsCorrect &&
                         !userResults.Any(ur => ur.QuestionId == q.Id && ur.AnswerId == a.Id);
                        var selectOk = a.IsCorrect &&
                         userResults.Any(ur => ur.QuestionId == q.Id && ur.AnswerId == a.Id);
                        var nonselectOk = a.IsCorrect &&
                         !userResults.Any(ur => ur.QuestionId == q.Id && ur.AnswerId == a.Id);
                        var correctness = selectOk ? ResultAnswerCorrectness.SelectCorrect :
                         nonselectOk ? ResultAnswerCorrectness.NonSelectCorrect :
                         neutral ? ResultAnswerCorrectness.Neutral : ResultAnswerCorrectness.NonCorrect;

                        return new ResultAnswerViewModel
                        {
                            Text = a.Text,
                            IsCorrect = correctness,
                        };
                    }).ToList()
                };
            }).ToList();

            return viewModel;
        }
    }
}