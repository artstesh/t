﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using StudentTest.Models;

namespace StudentTest.ViewModels
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Логин не введён")]
        public string Login { get; set; }
        [Required(ErrorMessage = "Пароль не введён", AllowEmptyStrings = false)]
        public string Password { get; set; }
        [Required(ErrorMessage = "Группа не выбрана")]
        public int GroupId { get; set; }
        public List<SelectListItem> Groups { get; set; }

        public LoginViewModel()
        {
            Groups = new List<SelectListItem>();
        }
        public LoginViewModel(List<Group> groups)
        {
            Groups = groups.Select(g => new SelectListItem()
            {
                Value = g.Id.ToString(), 
                Text = g.Name
            }).ToList();
        }
    }
}