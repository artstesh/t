using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace StudentTest.Models
{
    [Table("question")]
    public class Question : BaseEntity
    {
        [Column("Text")]
        public string Text { get; set; }
        [Column("ThemeId")]
        public int ThemeId { get; set; }

        public virtual ICollection<Answer> Answers { get; set; }
        public virtual Theme Theme { get; set; }
    }
}