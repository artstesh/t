﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StudentTest.Models;
using StudentTest.DbContext;

namespace StudentTest.Services.S_G_Ch
{
    public static class Sess_Guid_Chang
    {
        public static string NewSess_NewGuid_SaveChang(IDataContext _context, int Id)
        {
            var session = new Session();
            session.Date = DateTime.Now;
            session.Guid = Guid.NewGuid().ToString("N");
            session.UserId = Id;
            _context.Sessions.Add(session);
            _context.SaveChanges();
            return session.Guid;
        }
    }
}