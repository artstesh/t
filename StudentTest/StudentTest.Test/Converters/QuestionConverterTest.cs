using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using StudentTest.Converters;
using StudentTest.Models;
using StudentTest.Test.FakeFactories;
using StudentTest.ViewModels;

namespace StudentTest.Test.Converters
{
    public class QuestionConverterTest
    {
        [Test, AutoMoqData]
        public void ToModel_Question_Success(Question question, int sessionId)
        {
            QuestionViewModel result = question.ToModel(sessionId);
            //
            Assert.True(result.QuestionId == question.Id);
            Assert.True(result.QuestionText == question.Text);
            Assert.True(result.SessionId == sessionId);
        }
        
        [Test, AutoMoqData]
        public void ToModel_Answers_Success(Question question, int sessionId, Answer answer)
        {
            question.Answers = new List<Answer>{answer};
            //
            var result = question.ToModel(sessionId);
            //
            var resultAnswer = result.Answers.First();
            Assert.True(resultAnswer.Key == answer.Id);
            Assert.True(resultAnswer.Value == answer.Text);
        }
    }
}