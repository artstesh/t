﻿using System.Net;
using System.Net.Http;
using System.Web.Mvc;
using System.Web.Routing;
using StudentTest.Models;
using StudentTest.Services;
using StudentTest.ViewModels;
using StudentTest.Converters;
using System.Collections.Generic;

namespace StudentTest.Controllers
{
    public class ResultController : Controller
    {
        private readonly IUserService _userService;
        //private readonly QuestionService _questionService;

        public ResultController()
        {
            _userService = new UserService();
            //_questionService = new QuestionService();
        }

        public ActionResult Index()
        {
            List<UserViewModel> allUsers = _userService.AllUsers();
            return View(allUsers);
        }

        public ActionResult Sessions(int Id)
        {
            List<SessionViewModel> sessions = _userService.OneUser(Id);
            return View(sessions);
        }

        public ActionResult Results(string SessionGuid)
        {
            List<ResultViewModel> result = _userService.UserResults(SessionGuid);
            return View(result);
        }
    }
}