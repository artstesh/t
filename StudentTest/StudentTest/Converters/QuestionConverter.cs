using System.Collections.Generic;
using System.Linq;
using StudentTest.Models;
using StudentTest.ViewModels;

namespace StudentTest.Converters
{
    public static class QuestionConverter
    {
        /// <summary>
        /// https://metanit.com/sharp/tutorial/3.18.php
        /// </summary>
        /// <param name="question"></param>
        /// <param name="sesstionId"></param>
        /// <returns></returns>
        public static QuestionViewModel ToModel(this Question question, string sesstionGuid) 
        {
            var viewModel = new QuestionViewModel();
            viewModel.QuestionId = question.Id;
            viewModel.QuestionText = question.Text;
            viewModel.SessionGuid = sesstionGuid;
            viewModel.Answers = question.Answers.ToDictionary(k => k.Id, v => v.Text);
            return viewModel;
        }
    }
}