﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StudentTest.ViewModels
{
    public class ResultViewModel
    {
        public string QuestionText { get; set; }
        public List<ResultAnswerViewModel> Answers { get; set; }
    }
}