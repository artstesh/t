using System.Collections.Generic;
using StudentTest.Models;
using StudentTest.ViewModels;

namespace StudentTest.Services
{
    public interface IUserService
    {
        //string GetSessionGuid(string login);
        string GetSessionGuid(LoginViewModel loginViewModel);
        List<UserViewModel> AllUsers();
        List<SessionViewModel> OneUser(int Id);
        List<ResultViewModel> UserResults(string Guid);
    }
}