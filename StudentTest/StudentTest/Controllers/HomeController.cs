﻿using System.Net;
using System.Net.Http;
using System.Web.Mvc;
using System.Web.Routing;
using StudentTest.Models;
using StudentTest.Services;
using StudentTest.ViewModels;
using StudentTest.Converters;
using System.Collections.Generic;
using StudentTest.Models.NotTable;
using StudentTest.DbContext;
using System.Linq;


namespace StudentTest.Controllers
{
    public class HomeController : Controller
    {
        private readonly IUserService _userService;
        private readonly QuestionService _questionService;
        private readonly LoginService _loginService;
        private readonly ThemeService _themeService;

        public HomeController()
        {
            _userService = new UserService();
            _questionService = new QuestionService();
            _loginService = new LoginService();
            _themeService = new ThemeService();
        }

        public ActionResult Login()
        {
            List<Group> groups = _loginService.GetGroupsAtLoginPage();
            return View(new LoginViewModel(groups));
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel loginViewModel)
        {
            var sessionGuid = _userService.GetSessionGuid(loginViewModel);
            if (sessionGuid == null)
            {
                return RedirectToAction("Login"); // пароль не введён
            }
            else
            {
                return RedirectToAction("Prestart", new { sessionGuid });
            }
        }

        [HttpGet]
        public ActionResult Prestart(string sessionGuid)
        {
            var themes = _themeService.GetThemes(sessionGuid);
            return View(themes);
        }

        [HttpGet]
        public ActionResult Test(string sessionGuid, int? themeId = null)
        {
            var question = (themeId != null) 
                ? _questionService.StartSession(sessionGuid, (int)themeId)
                : _questionService.NextQuestion(sessionGuid);
            if (question == null)
                return RedirectToAction("Finish", new {sessionGuid});
            return View(question);
        }

        [HttpPost]
        public ActionResult Test(ResultModel model)
        {
            _questionService.SaveResult(UserResultConverter.ToEntity(model));
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpGet]
        public ActionResult Finish(string sessionGuid)
        {
            return View();
        }
    }
}