﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StudentTest.ViewModels
{
    public enum ResultAnswerCorrectness
    {
        SelectCorrect, NonSelectCorrect, Neutral, NonCorrect
    }

    public enum ResultAnswerRightness
    {
        Right, Wrong
    }

    public class ResultAnswerViewModel
    {
        public string Text { get; set; }
        public ResultAnswerCorrectness IsCorrect { get; set; }
    }
}