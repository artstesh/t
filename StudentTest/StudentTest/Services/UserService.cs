using System;
using System.Linq;
using StudentTest.DbContext;
using StudentTest.Models;
using StudentTest.ViewModels;
using System.Collections.Generic;
using StudentTest.Converters;
using StudentTest.Services.S_G_Ch;
using System.Web.Mvc;

namespace StudentTest.Services
{
    public class UserService : IUserService
    {
        private readonly IDataContext _context;

        public UserService(IDataContext context = null)
        {
            _context = context ?? new DataContext();
        }

        public string GetSessionGuid(LoginViewModel loginViewModel) // ��������� ������ ��� �� ������ ������, � ������ ������ ������ ������ �����
        {
            string login = loginViewModel.Login;
            string hashPassword = Encryption.Encryption.GetHashString(loginViewModel.Password);
            int groupId = loginViewModel.GroupId;
            List<SelectListItem> groups = loginViewModel.Groups;

            var existingUser = _context.Users.FirstOrDefault(u => u.Login == login);
            var existingPassword = _context.Users.FirstOrDefault(u => u.Password == hashPassword);
            var existingGroupId = _context.Users.FirstOrDefault(u => u.GroupId 
                == _context.Groups.Select(g => g.Id).FirstOrDefault());

            if (existingUser == null)
            {
                var user = _context.Users.Add(new User
                {
                    Login = login,
                    Password = Encryption.Encryption.GetHashString(hashPassword),
                    GroupId = _context.Groups.Where(g => g.Id == groupId).Select(g => g.Id).FirstOrDefault()
                });
                _context.SaveChanges();
                //var session = new Session();
                //session.Date = DateTime.Now;
                //session.Guid = Guid.NewGuid().ToString("N");
                //session.UserId = user.Id;
                //_context.Sessions.Add(session);
                //_context.SaveChanges();
                //return session.Guid;
                return Sess_Guid_Chang.NewSess_NewGuid_SaveChang(_context, user.Id);
            }
            else if ((existingUser.Password == hashPassword) 
                && (existingUser.GroupId == _context.Groups.Where(g =>
                g.Id == groupId).Select(g => g.Id).FirstOrDefault()))
            {
                //var session = new Session();
                //session.Date = DateTime.Now;
                //session.Guid = Guid.NewGuid().ToString("N");
                //session.UserId = existingUser.Id;
                //_context.Sessions.Add(session);
                //_context.SaveChanges();
                //return session.Guid;
                return Sess_Guid_Chang.NewSess_NewGuid_SaveChang(_context, existingUser.Id);
            }
            else
            {
                return null;
            }
        }

        public List<UserViewModel> AllUsers()
        {
            List<User> allUsers = _context.Users.Distinct().ToList();
            //return UserConverter.ToModel(allUsers);
            return allUsers.ToModel();
        }

        public List<SessionViewModel> OneUser(int userId)
        {
            List<string> oneUserAllSessionsGuids = _context.Sessions
                .Where(u => u.UserId == userId).Select(g => g.Guid).ToList();
            //return SessionConverter.ToModel(oneUserAllSessionsGuids, userId);
            return oneUserAllSessionsGuids.ToModel();
        }

        public List<ResultViewModel> UserResults(string Guid)
        {
            List<UserResult> usrRes = _context.UserResults.Where(q => q.SessionGuid == Guid).ToList();
            var questionIds = usrRes.Select(q => q.QuestionId).ToList();
            var correctResults = _context.Questions.Where(q => questionIds.Contains(q.Id)).ToList();
            return usrRes.ToModel(correctResults);
        }
    }
}