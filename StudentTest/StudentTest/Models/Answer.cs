using System.ComponentModel.DataAnnotations.Schema;

namespace StudentTest.Models
{
    [Table("answer")]
    public class Answer : BaseEntity
    {
        [Column("Text")]
        public string Text { get; set; }
        [Column("IsCorrect")]
        public bool IsCorrect { get; set; }
        [Column("QuestionId")]
        public int QuestionId { get; set; }
        [Column("ThemeId")]
        public int ThemeId { get; set; }

        public virtual Question Question { get; set; }
        public virtual Theme Theme { get; set; }
    }
}