﻿using System.Data.Entity;
using MySql.Data.EntityFramework;
using StudentTest.Models;

namespace StudentTest.DbContext
{
    [DbConfigurationType(typeof(MySqlEFConfiguration))]
    public class DataContext : System.Data.Entity.DbContext, IDataContext
    {
        public DataContext() : base("DataContext")
        {
        }

        public DbSet<Answer> Answers { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Session> Sessions { get; set; }
        public DbSet<SessionQuestion> SessionQuestions { get; set; }
        public DbSet<UserResult> UserResults { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Theme> Themes { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("public");
            base.OnModelCreating(modelBuilder);
        }

        public override DbSet<T> Set<T>()
        {
            return base.Set<T>();
        }
    }
}