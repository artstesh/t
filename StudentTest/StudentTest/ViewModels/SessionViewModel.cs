﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StudentTest.ViewModels
{
    public class SessionViewModel
    {
        //public int UserId { get; set; }
        public string SessionGuid { get; set; }

        public DateTime DateTime { get; set; }
    }
}