using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using SemanticComparison.Fluent;
using StudentTest.DbContext;
using StudentTest.Models;
using StudentTest.Services;
using StudentTest.Test.Context;
using StudentTest.Test.FakeFactories;

namespace StudentTest.Test.Services
{
    public class QuestionServiceTest
    {
        private QuestionService _service;
        private IDataContext _context;
    
        [SetUp]
        public void SetUp()
        {
            _context = new FakeContext();
            _service = new QuestionService(_context);
        }

       [Test, AutoMoqData]
        public void StartSession_Success_Saving(int sessionId, List<Question> questions)
        {
            _context.Questions.AddRange(questions);
            //
            _service.StartSession(sessionId);
            //
            var saved = _context.SessionQuestions.Count();
            Assert.True(saved == 2);
        }

       [Test, AutoMoqData]
        public void StartSession_Returns_Question(int sessionId, Question question)
        {
            _context.Questions.Add(question);
            //
            var result = _service.StartSession(sessionId);
            //
            
            Assert.True(result.QuestionId == question.Id);
        }

       [Test, AutoMoqData]
        public void NextQuestion_Success_Saving(int sessionId, List<Question> questions)
        {
            _context.Questions.AddRange(questions);
            var sessionQuestions = questions.Select(q => new SessionQuestion
                {Question = q, QuestionId = q.Id, SessionId = sessionId});
            _context.SessionQuestions.AddRange(sessionQuestions);
            //
            _service.NextQuestion(sessionId);
            //
            var saved = _context.SessionQuestions.Count();
            Assert.True(saved == 2);
        }
    }
}