using System.Linq;
using NUnit.Framework;
using StudentTest.DbContext;
using StudentTest.Models;
using StudentTest.Services;
using StudentTest.Test.Context;
using StudentTest.Test.FakeFactories;

namespace StudentTest.Test.Services
{
    [TestFixture]
    public class UserServiceTest
    {
        private UserService _service;
        private IDataContext _context;
    
        [SetUp]
        public void SetUp()
        {
            _context = new FakeContext();
            _service = new UserService(_context);
        }

        [Test, AutoMoqData]
        public void GetSessionId_New_User(string login)
        {
            _service.GetSessionGuid(login);
            //
            var expectedUser = _context.Users.First();
            Assert.True(expectedUser.Login == login);
        }

        [Test, AutoMoqData]
        public void GetSessionId_Correct_New_Session(string login)
        {
            var result = _service.GetSessionGuid(login);
            //
            var expectedSession = _context.Sessions.First();
            Assert.True(expectedSession.Id == result);
        }

        [Test, AutoMoqData]
        public void GetSessionId_Correct_UserId_For_New(string login)
        {
            _service.GetSessionGuid(login);
            //
            var expectedUser = _context.Users.First();
            var expectedSession = _context.Sessions.First();
            Assert.True(expectedSession.UserId == expectedUser.Id);
        }

        [Test, AutoMoqData]
        public void GetSessionId_Correct_UserId_For_Existing(string login, User user)
        {
            _context.Users.Add(user);
            //
            _service.GetSessionGuid(user.Login);
            //
            var expectedSession = _context.Sessions.First();
            Assert.True(expectedSession.UserId == user.Id);
        }
    }
}