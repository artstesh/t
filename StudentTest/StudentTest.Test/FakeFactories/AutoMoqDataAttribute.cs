using AutoFixture.NUnit3;

namespace StudentTest.Test.FakeFactories
{
    public class AutoMoqDataAttribute : AutoDataAttribute
    {
        public AutoMoqDataAttribute() : base(FakeFactory.Fixture)
        {
        }
    }
}