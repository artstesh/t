﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StudentTest.Models;
using StudentTest.ViewModels;

namespace StudentTest.Converters
{
    public static class UserConverter
    {
        /// https://metanit.com/sharp/tutorial/3.18.php
        /// https://metanit.com/sharp/tutorial/4.5.php

        public static List<UserViewModel> ToModel(this List<User> users)
        {
            List<UserViewModel> viewModel = new List<UserViewModel>();

            foreach (var user in users)
            {
                viewModel.Add(new UserViewModel { Id = user.Id, Login = user.Login});
            }

            return viewModel;
        }
    }
}